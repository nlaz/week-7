#include "chrono.h"
#include <iostream>
#include <stack>
#include <unordered_map>


bool validString(const std::string& input) {
  std::stack<char> stack;
  std::unordered_map<char, char> parenthesesMap {
    {')', '('},
    {'}', '{'},
    {']', '['}
  };

  for (char c : input) {
    if (c == '(' || c == '{' || c == '[') {
        stack.push(c);
    } else if (c == ')' || c == '}' || c == ']') {
        if (stack.empty() || stack.top() != parenthesesMap[c]) {
        return false;
      } else {
        stack.pop(); 
      }
    }
  }
  return stack.empty();
}

int main() {
  std::string input;
  std::cout << "Введите строку: ";
  std::cin >> input;

  steady_time_point start = chrono::steady_clock::now();

  if (validString(input)) {
    std::cout << "Строка валидна" << std::endl;
  } else {
    std::cout << "Строка невалидна" << std::endl;
  }
   steady_time_point end = chrono::steady_clock::now();

  milliseconds elapsed = end - start;

   std::cout << "Программа выполнялась " << elapsed.count() << " миллисекунд" << std::endl;

  return 0;
}
