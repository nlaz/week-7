#ifndef CHRONO_H
#define CHRONO_H

#include <chrono> 

namespace chrono = std::chrono;

using seconds = chrono::duration<double>;
using milliseconds = chrono::duration<double, std::milli>;
using microseconds = chrono::duration<double, std::micro>;
using nanoseconds = chrono::duration<double, std::nano>;

using system_time_point = chrono::time_point<chrono::system_clock>;
using steady_time_point = chrono::time_point<chrono::steady_clock>;
using high_resolution_time_point = chrono::time_point<chrono::high_resolution_clock>;

#endif 
